module gitee.com/changeden/dubbo-go-middleware-database

go 1.16

require (
	dubbo.apache.org/dubbo-go/v3 v3.0.1
	gitee.com/changeden/dubbo-go-starter v0.1.4
	gorm.io/driver/clickhouse v0.3.1
	gorm.io/driver/mysql v1.3.2
	gorm.io/driver/postgres v1.3.1
	gorm.io/driver/sqlserver v1.3.1
	gorm.io/gorm v1.23.1
)
